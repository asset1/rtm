<?php
if(!isset($_POST['submit']))
{
    //This page should not be accessed directly. Need to submit the form.
    echo "error; you need to submit the form!";
}
$visitor_email = $_POST['email'];

//Validate first
if(empty($visitor_email))
{
    echo "Email are mandatory!";
    exit;
}

$email_from = 'info@rtm.kz';
$email_subject = 'New form submission';
$email_body = 'You have received a new email, email address: '.$visitor_email;

$to = "aset@kineyev.com";
$headers = "From: $email_from \r\n";

mail($to,$email_subject,$email_body,$headers);
header('Location:index.html');
